//server
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace sv
{
    class Program
    {
       
        private static void cal(object arg)
        {
            TcpClient client = (TcpClient)arg;

            try
            {
                StreamReader read = new StreamReader(client.GetStream());
                StreamWriter write = new StreamWriter(client.GetStream());

                string operasi = string.Empty;
                int nilai1 = 0, nilai2 = 0, hasil = 0;

                while (true)
                {
                    string data1 = read.ReadLine();
                    nilai1 = Convert.ToInt32(data1);
                    Console.WriteLine("\n Nilai 1      : " + nilai1);

                    operasi = read.ReadLine();
                    Console.WriteLine(" Operasi     : " + operasi);

                    string data2 = read.ReadLine();
                    nilai2 = Convert.ToInt32(data2);
                    Console.WriteLine(" Nilai 2        : " + nilai2);

                    if (operasi == "+")
                    {
                        hasil = nilai1 + nilai2;
                    }
                    if (operasi == "-")
                    {
                        hasil = nilai1 - nilai2;
                    }
                    if (operasi == "*")
                    {
                        hasil = nilai1 * nilai2;
                    }
                    if (operasi == "/")
                    {
                        hasil = nilai1 / nilai2;
                    }

                    string has = Convert.ToString(hasil);
                    write.WriteLine(" Hasil      : " + has);
                    write.Flush();
                }
            }
            catch (IOException)
            {
                Console.WriteLine("\n Client exit");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }
        public static void Main()
        {
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
                listener.Start();

                Console.WriteLine(" MultiTreadedEchoServer Started...");
                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine(" New Client...]");
                    Thread t = new Thread(cal);

                    t.Start(client);
                }
            }
            catch (Exception)
            {
                Console.WriteLine();
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }


        }

    }
}