// client
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

public class Client
{
 public static void Main()
 {
  try
  {
   TcpClient client = new TcpClient("127.0.0.1", 8080);
   Console.WriteLine(" entered");

   StreamReader read = new StreamReader(client.GetStream());
   StreamWriter write = new StreamWriter(client.GetStream());
   string nilai1, nilai2, operasi;a

   while (true)
   {
    nilai1 = Console.ReadLine();
    Console.Write(" \n Nilai 1 : ");
    write.WriteLine(nilai1);
    write.Flush();

    operasi = Console.ReadLine();
    Console.Write(" Operasi : ");
    write.WriteLine(operasi);
    write.Flush();

    Console.Write(" Nilai 2 : ");
    nilai2 = Console.ReadLine();
    write.WriteLine(nilai2);
    write.Flush();

    string hasill = read.ReadLine();
    Console.WriteLine(hasill);
   }

   read.Close();
   write.Close();
   client.Close();
  }
  catch (Exception)
  {
   Console.WriteLine();
  }
 }
}